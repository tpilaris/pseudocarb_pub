On The Structure, Magnetic Properties, And Infrared Spectra Of Iron Pseudocarbynes In The Interstellar Medium
PILARISETTY TARAKESHWAR,1 PETER R. BUSECK,1, 2 AND F. X. TIMMES2
1School of Molecular Sciences, Arizona State University, Tempe, Arizona 85287-1604, USA
2School of Earth and Space Exploration, Arizona State University, Tempe, Arizona 85287-6004, USA


List of structure (*.xyz) and spectra (*.dat) files corresponding to the figures shown in the manuscript.

c2h2.xyz
c10h2.xyz
c4h2.xyz
c2h2_ir.dat
c10h2_ir.dat
c4h2_ir.dat
c6h2.xyz
c6h2_ir.dat
c8h2.xyz
c8h2_ir.dat
c9nh.xyz
c9nh_ir.dat
fe12_c10h2_ms39.xyz
fe12_c10h2_ms39_ir.dat
fe12_c10h2_ms38.xyz
fe12_c10h2_ms38_ir.dat
fe12_c10h2_ms37.xyz
fe12_c10h2_ms37_ir.dat
fe12_c10h2_ms36.xyz
fe12_c10h2_ms36_ir.dat
fe13_c6h2_ms41_ir.dat
fe13_c6h2_ms39_ir.dat
fe13_c6h2_ms41.xyz
fe13_c6h2_ms39.xyz
fe13_c6h2_ms39_bp86_ir.dat
fe13_c6h2_ms39_bp86.xyz
fe13_c6h2_ms41_bp86.xyz
fe13_c6h2_ms41_bp86_ir.dat
fe13_6c2h2_ms35_ir.dat
fe13_6c2h2_ms35.xyz
fe13_c2h2_ms41.xyz
fe13_c2h2_ms41_ir.dat
fe12_c2h2_ms41.xyz
fe12_c2h2_ms41_ir.dat
fe13_6c2h2_ms35_bp86_ir.dat
fe13_6c2h2_ms35_bp86.xyz
fe13_c2h2_ms41_bp86.xyz
fe13_c2h2_ms41_bp86_ir.dat
